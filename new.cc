using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Markup;
using Microsoft.VisualBasic.CompilerServices;

namespace PCL
{
	// Token: 0x02000037 RID: 55
	[DesignerGenerated]
	public partial class PageOtherTest : AdornerDecorator, IComponentConnector
	{
		// Token: 0x060003C5 RID: 965 RVA: 0x00022698 File Offset: 0x00020898
		private void BtnTarget_Click(object sender, EventArgs e)
		{
			ModMain.HintType[] array = new ModMain.HintType[3];
			array[0] = ModMain.HintType.Critical;
			array[1] = ModMain.HintType.Finish;
			ModMain.HintType type = (ModMain.HintType)Conversions.ToInteger(ModBase.RandomOne(array));
			string text = Conversions.ToString(ModBase.RandomOne(new object[]
			{
				ModBase.RandomOne(new string[]
				{
					"> $",
					"Output: $.",
					"* $",
					">> $.",
					"{text:\"$.\"}",
					"[LTCat] $.",
					"<LTCat> $."
				}),
				ModBase.RandomOne(new string[]
				{
					"$！$！",
					"$$$！",
					"$，就是$.",
					"$，$，$！",
					"$，$！",
					"$！是$！",
					"$ $ $"
				}),
				ModBase.RandomOne(new string[]
				{
					"就是$啦.",
					"大概是$什么的吧.",
					"出现了！$！",
					"这是$么……",
					"原来是$.",
					"嘿，我发现了$！",
					"总之，$.",
					"输出：$.",
					"以下为$.",
					"该不会是$吧？",
					"你相信吗？这竟然是$.",
					"难以置信！这是$.",
					"这是$.",
					"如果是$的话……",
					"绝不可能有$！"
				}),
				ModBase.RandomOne(new string[]
				{
					"灰色是个谎言.",
					"点一下不够就点两下.",
					"开开关关开开关关开开关关.",
					"今日人品：100.",
					"属于宇宙的数字.",
					"鱼人节快乐.",
					"滑稽节是一个节日呢.",
					"ASCII 总是三位数.",
					"帮助的英文是 Help.",
					"砸反馈就完事了.",
					"丢骰子能带来灵感.",
					"！读着倒要话候时有",
					"从罗马开始.",
					"MCBBS 的本体是箱子.",
					"化学，文档，网格.",
					"网址就是来路.",
					"越过屏障.",
					"卢恩与去路.",
					"地下埋藏着宝藏.",
					"从老线索中发现新东西.",
					"重组碎片……",
					"橙色线，藏着线和点.",
					"于历史中发掘秘密.",
					"线索在游戏之外.",
					"穷举不能让你变得更强.",
					"OBSIDIAN.",
					"深蓝色的极客.",
					"不要忽视背景.",
					"GIF.",
					"结束了？开始了。"
				}),
				"$.",
				ModBase.RandomOne(new string[]
				{
					"来硬的！",
					"钻石！",
					"我们需要再深入些！",
					"结束了？",
					"见鬼去吧！",
					"君临天下！",
					"与火共舞！",
					"本地的酿造厂！",
					"为什么会变成这样呢？",
					"信标工程师！",
					"不稳定的同盟！",
					"天空即为极限！",
					"甜蜜的梦！",
					"探索的时光！",
					"狙击手的对决！",
					"这是？工作台！",
					"永恒的伙伴！",
					"腥味十足的生意！",
					"结束了。",
					"开始了？",
					"这交易不错！",
					"你的世界.",
					"/summon Creeper ~ ~ ~ {Fuse:0}",
					"MC-98587.",
					"紫黑格子波纹疾走.",
					"命令方块不适合作为武器.",
					"mjsb.",
					"新增了一堆 Bug.",
					"Sky Factory 4.",
					"这是刻意的游戏设计.",
					"附魔神圣橡胶树树苗.",
					"有频道的 AE 不是好 AE.",
					"你好中国！",
					"/give @a hugs 64.",
					"这是特性.",
					"awwww man.",
					"Creeper?",
					"Minecraft 2.0.",
					"Hello, Herobrine.",
					"Herobrine...xia?",
					"It's a FEATURE.",
					"我 Mojang 绝不跳票.",
					"苦力怕！不是爬行者！",
					"蠢虫？毒虫？蠹虫？",
					"粉色羊是隐性纯合子.",
					"Can't keep up!",
					"比钻石更强."
				}),
				"$.",
				ModBase.RandomOne(new string[]
				{
					"午时已到.",
					"钓鱼有风险.",
					"Deadline 是第一生产力.",
					"你的系统反正就是需要更新，你是要重启呢还是重启呢.",
					"找不到对象.",
					"妈的智障.",
					"面露异常.",
					"Nothing is true, everything is feature.",
					"按 [E] 以赛艇.",
					"新名单！新名单！",
					"夹.",
					"这个游戏没有 SSR.",
					"光敏性癫痫警告.",
					"CO~ CO~ DA~ YO~",
					"众所周知，炉石传说是免费游戏.",
					"你号没了.",
					"王八坨子号.",
					"A 级记忆删除（物理）.",
					"Also try SCP-CN-660-J.",
					"Command Block Logic.",
					"Also try SCP-CN-048-J.",
					"Also try 丰裕之角.",
					"任天堂就是世界的主宰.",
					"不要停下来啊！",
					"To be continued.",
					"脑子放假去了.",
					"抱抱是第一生产力.",
					"滚你.",
					"当你觉得我咕了，但是我没咕，这也是一种咕.",
					"危.",
					"十个隐藏主题.",
					"HMSL.",
					"心脏停跳文学社.",
					"我 Forge 打开这么慢一定是你启动器出 Bug 了.",
					"50382.",
					"吓得我都把基岩挖掉了.",
					"那就当做没有 Bug 好了.",
					"Crazy Boy Love.",
					"向骰子低头.",
					"发出咕咕咕的声音.",
					"Determination.",
					"五十人解密加强连.",
					"PPCCLL.",
					"暴咕无人机.",
					"Early Access."
				}),
				"$.",
				ModBase.RandomOne(new string[]
				{
					"Also try 艾列之书.",
					"Also try Celeste.",
					"Also try The Witness.",
					"Also try 防御工厂.",
					"Also try gamejam.",
					"破产了启动器",
					"泡菜龙启动器.",
					"劈柴驴启动器.",
					"碰瓷狼启动器.",
					"可随手机壳变化颜色.",
					"要恰饭的嘛.",
					"爆炸就是艺术，当量就是正义.",
					"帅得体面，猫得明白.",
					"旷野之息天下第一.",
					"FLAG IS WIN.",
					"凌波微步，快乐的舞步！",
					"超高校级的咕咕咕.",
					"呐呐呐.",
					"倒一杯卡布奇诺.",
					"59 区废墟的红刀哥.",
					"Hello, world.",
					"把龙猫拖去祭天.",
					"五颜六色的黑.",
					"五光十色的白.",
					"高 Ping 战士.",
					"一脸玄素.",
					"最强的法术是掉线法术.",
					"我能怎么办啊，我也很绝望啊.",
					"保护我的敌人，痛击我的队友.",
					"掉帧是道具吗.",
					"你不是挂怎么和我们打.",
					"叮叮叮叮.",
					"无阻力滚轮.",
					"发出死了的声音.",
					"打不过就丢人，骂人.",
					"Professional Crush Launcher.",
					"Alpha 之后是 Beta.",
					"奥利给.",
					"战神与他的文盲老父亲.",
					"锟斤拷烫烫烫.",
					"土豆怎么烹调才好吃.",
					"Missing No.",
					"IO 效果之箭.",
					"龙猫战争就是龙猫战争.",
					"我跟你讲你这样是要被夹的."
				})
			}));
			string newValue = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(ModBase.RandomOne(new string[]
			{
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"一只",
				"一条",
				"一个",
				"一段",
				"几个"
			}), ModBase.RandomOne(new string[]
			{
				"",
				"随机的",
				"瞎搞的",
				"提示性的",
				"丢骰子的",
				"",
				"龙猫写的",
				"搞笑的",
				"打发时间的",
				"引人深思的",
				"",
				"野生的",
				"有逻辑的",
				"摸鱼的",
				"富含哲学的"
			})), ModBase.RandomOne(new string[]
			{
				"",
				"",
				"",
				"",
				"沙雕",
				"沙雕",
				"傻啦吧唧",
				"奇葩",
				"",
				"",
				"",
				"",
				"鬼畜",
				"根本停不下来的",
				"制杖"
			})), ModBase.RandomOne(new string[]
			{
				"",
				"",
				"",
				"",
				"",
				"",
				"测试",
				"测试",
				"彩蛋"
			})), ModBase.RandomOne(new string[]
			{
				"信息",
				"提示",
				"文本",
				"字符串",
				"弹出提示",
				"消息"
			})));
			string newValue2 = Conversions.ToString(ModBase.RandomOne(new object[]
			{
				"！",
				"！",
				"！",
				"！",
				"。",
				"",
				ModBase.RandomOne(new string[]
				{
					"……",
					"…………",
					"（",
					"~"
				}),
				ModBase.RandomOne(new string[]
				{
					" =w=",
					"（小声",
					" =.=",
					" =A=",
					" OwO",
					"（震声",
					" #Y%&(^$#"
				})
			}));
			string text2 = text.Replace("$", newValue).Replace(".", newValue2);
			if (text2.Count<char>() <= 14 && text2.Count<char>() >= 6)
			{
				int num = ModBase.RandomInteger(1, 30);
				if (num <= 4)
				{
					if (num != 1)
					{
						if (num == 4)
						{
							string text3 = "";
							foreach (char value in text2)
							{
								text3 += Conversions.ToString(value);
								text3 += " ";
							}
							text2 = text3;
						}
					}
					else
					{
						text2 = new string(text2.Reverse<char>().ToArray<char>());
					}
				}
				else if (num != 7)
				{
					if (num == 10)
					{
						string text5 = "";
						foreach (char c in text2)
						{
							text5 = Conversions.ToString(Operators.AddObject(text5, ModBase.RandomOne(new object[]
							{
								c,
								c,
								c,
								Operators.ConcatenateObject(ModBase.RandomOne("qwertyuiopasdfghjklzxcvbnm".ToCharArray()), ModBase.RandomOne("QWERTYUIOPASDFGHJKLZXCVBNM".ToCharArray())),
								Operators.ConcatenateObject(ModBase.RandomOne("QWERTYUIOPASDFGHJKLZXCVBNM".ToCharArray()), ModBase.RandomOne("0123456789".ToCharArray())),
								Operators.ConcatenateObject(ModBase.RandomOne("0123456789".ToCharArray()), ModBase.RandomOne("!@#$%^&*()[]|\\:;'<>?/".ToCharArray())),
								Operators.ConcatenateObject(ModBase.RandomOne("!@#$%^&*()[]|\\:;'<>?/".ToCharArray()), ModBase.RandomOne("qwertyuiopasdfghjklzxcvbnm".ToCharArray()))
							})));
						}
						text2 = text5;
					}
				}
				else
				{
					string text7 = "";
					foreach (char value2 in text2)
					{
						text7 = Conversions.ToString(Operators.AddObject(text7, ModBase.RandomOne(new string[]
						{
							Conversions.ToString(value2),
							Conversions.ToString(value2),
							Conversions.ToString(value2),
							"■",
							"■",
							"[数据删除]"
						})));
					}
					text2 = text7;
				}
			}
			text2 = text2.Trim(new char[]
			{
				' '
			});
			if (Operators.CompareString(text2, "", true) == 0)
			{
				text2 = Conversions.ToString(ModBase.RandomOne(new string[]
				{
					"什么都没有。",
					"无事发生。",
					"Null.",
					"Nothing.",
					"Error 404: Not found.",
					"Null Reference Exception."
				}));
			}
			ModMain.Hint(text2, type, true);
		}
	}
}
